const ratedCat = "show-hidden-rated";
const horrorCat = "show-hidden-horror";
const biographyCat = "show-hidden-biography";
const choiceCat = "show-hidden-choice";
const filterImdb = "http://localhost:8000/api/v1/titles/?sort_by=-imdb_score";
const filterHorror = "http://localhost:8000/api/v1/titles" +
                     "/?sort_by=-imdb_score&genre=horror";
const filterBiography = "http://localhost:8000/api/v1/titles" +
                        "/?sort_by=-imdb_score&genre=Biography";
const gridRated = ".grid-film__img-bloc-rated";
const gridHorror = ".grid-film__img-bloc-horror";
const gridBiography = ".grid-film__img-bloc-biography";
const gridImg = "img.grid-film__img";
const gridTitle = "h3.grid-film__film-title";
const gridGenre = "http://localhost:8000/api/v1/titles" +
                  "/?sort_by=-imdb_score&genre=";

// Get best movie of API
function getDataBestFilm() {
    const titleElement = document.getElementById("bestmovie_title");
    const imgElement = document.getElementById("bestmovie_img");
    getData("http://localhost:8000/api/v1/titles/?sort_by=-imdb_score")
    .then(function(data) {
        let bestFilmTitle = data.results[0].title;
        let bestFilmImg = data.results[0].image_url;
        let bestFilmUrl = data.results[0].url;
        titleElement.textContent = bestFilmTitle;
        imgElement.setAttribute("src", bestFilmImg);
        return bestFilmUrl;
    })
    .then(function(movieUrl) {
        getBestFilm(movieUrl);
    });
}


// Get a movie description
function getBestFilm(url) {
    const elemDescription = document.getElementById("desc-best");
    getData(url).then(function(data) {
        let description = data.description;
        elemDescription.textContent = description;
    });
}


// Get 6 url of movie for display in grid html
async function getGridFilm(url) {
    const urlScore = "http://localhost:8000/api/v1/titles/?sort_by=-imdb_score";
    let listedUrl = [];
    const data = await getData(url);
    let verif = true;
    while (verif === true) {
        if (url === urlScore) {
            for (let i = 0; i < data.results.length; i++) {
                if (i === 0) {}
                else {
                    listedUrl.push(data.results[i].url)
                }
            }
            if (data.next === null) {
                verif = false
            } else {
                const dataNext = await getData(data.next)
                for (let p = 0; p < 2; p++) {
                    listedUrl.push(dataNext.results[p].url)
                }
                verif = false
            }
        } else {
            for (let i = 0; i < data.results.length; i++) {
                listedUrl.push(data.results[i].url)
            }
            if (data.next === null) {
                verif = false
            } else {
                const dataNext = await getData(data.next)
                listedUrl.push(dataNext.results[0].url)
                verif = false
            }
        }
    }
    return listedUrl
}


// Get data of films and update it on HTML page
async function getDataGridFilm(url, class1, class2, class3) {
    let listedUrl = await getGridFilm(url)
    const gridFilmElem = document.querySelectorAll(class1)
    let p = 0
    let t = 0
    let imgList = []
    let titleList = []
    for (let i = 0; i < listedUrl.length; i++) {
        const data = await getData(listedUrl[i])
        let imgHd = imageHd(data.image_url)
        imgHd = await verificator(imgHd)
        imgList.push(imgHd)
        titleList.push(data.title)
    }
    gridFilmElem.forEach((gridFilmElem) => {
        const imgElem = gridFilmElem.querySelectorAll(class2)
        const titleElem = gridFilmElem.querySelectorAll(class3)
        imgElem.forEach((imgElem) => {
            imgElem.src = imgList[p]
            p++
        })
        titleElem.forEach((titleElem) => {
            titleElem.textContent = titleList[t]
            t++
        })
    })
}


// Getting a High resolution of movie image
function imageHd (urlImage) {
    let imgHd = urlImage.replace(/\.jpg$/, "")
    return imgHd
}


// Get list of genres available on API
async function getGenres() {
    let url = "http://localhost:8000/api/v1/genres/";
    let listGenres = [];
    let verif = true;
    while (verif === true) {
        await getData(url).then(function(data) {
            for (let i = 0; i < data.results.length; i++) {
                listGenres.push(data.results[i].name)
            }
            let nextUrl = data.next
            if (nextUrl === null) {
                verif = false
            } else {
                url = nextUrl
            }
        })
    }
    return listGenres
}


// Create balise option with name of genre
async function giveGenres() {
    await getGenres().then(function(data) {
        let selectElem = document.getElementById("list-category");
        for (let i = 0; i < data.length; i++) {
            let newElem = document.createElement("option")
            newElem.setAttribute("value", data[i])
            newElem.textContent = data[i]
            selectElem.appendChild(newElem)
        }
    })
}


// Detects which category has been choose
async function getCategoryChoose() {
    const selectElem = document.getElementById("list-category")
    await getGenres().then(function(data) {
        let listCategories = data
        return listCategories
    })
    .then(function(liste) {
        selectElem.addEventListener("change", async function finder() {
            clearMovie()
            let selectValue = selectElem.value
            let imgBloc = ".grid-film__img-bloc-choice"
            let filmImg = "img.grid-film__img"
            let filmTitle = "h3.grid-film__film-title"
            if (liste.includes(selectValue)) {
                let uri = gridGenre+selectValue
                await getDataGridFilm(uri, imgBloc, filmImg, filmTitle)
                displayGrid()
                showButtonChoice()
                console.log("Catégorie " + selectValue + " sélectionné")
            }
        })
    })
}


// Clear movie of grid choice
function clearMovie () {
    const titleElem = document.querySelectorAll("h3.title-choice")
    titleElem.forEach((title) => {
        title.textContent = "N/A"
    })
}


// check if image available, if not return other image
async function verificator (urlImage) {
    const img = new Image()
    img.src = urlImage
    try {
        await new Promise((resolve, reject) => {
            img.onload = () => resolve()
            img.onerror = () => reject()
        })
        return urlImage
    } catch (error) {
        return "../images/not_available.jpg"
    }
}


// Button show more for choice category appear
function showButtonChoice () {
    const button = document.getElementById("show-more-choice")
    button.style.display = "block"
}


// Display grid of movie (category choosing) when user select category
function displayGrid () {
    const gridChoice = document.querySelector(".grid-choice")
    const titleElem = document.querySelectorAll("h3.title-choice")
    let nbrGrid = 1
    titleElem.forEach((title) => {
        if (title.textContent === "N/A" || title.textContent === "") {
            let gridElems = document.getElementById("grid-" + nbrGrid)
            gridElems.style.visibility = "hidden"
        } else {
            let gridElems = document.getElementById("grid-" + nbrGrid)
            gridElems.style.visibility = "visible"
        }
        nbrGrid++
    })
    gridChoice.style.display = "grid"
}


// Change CSS for view or hidden content on Tablet
function changeStyleTablet (buttonState, category) {
    const hiddenTablet = " .grid-film__hidden-tablet"
    const hiddenMobile = " .grid-film__hidden-mobile"
    const changeElem = document.querySelector(category)
    const changeGrid = document.querySelectorAll(category + hiddenTablet)
    const changeGridMobile = document.querySelectorAll(category + hiddenMobile)
    const resolution = window.innerWidth
    if (buttonState === "Voir plus") {
        if (resolution <= 1024 && resolution > 430) {
            changeGrid.forEach((displayin) => {
                displayin.style.display = "block"
            })
            changeElem.style["grid-template-rows"] = "repeat(3, 1fr)";
        } else if (resolution <= 430) {
            changeGridMobile.forEach((displayin) => {
                displayin.style.display = "block"
            })
            changeElem.style["grid-template-rows"] = "repeat(6, 1fr)"
        }
    } else {
        if (resolution <= 1024 && resolution > 430) {
            changeGrid.forEach((displayin) => {
                displayin.style.display = "none"
            })
            changeElem.style["grid-template-rows"] = "repeat(2, 1fr)"
        } else if (resolution <= 430) {
            changeGridMobile.forEach((displayin) => {
                displayin.style.display = "none"
            })
            changeElem.style["grid-template-rows"] = "repeat(2, 1fr)"
        }
    }
}


// Detect click on button to show or hidden contents
function seeMore () {
    const seeMoreRated = document.getElementById(ratedCat)
    const seeMoreHorror = document.getElementById(horrorCat)
    const seeMoreBiography = document.getElementById(biographyCat)
    const seeMoreChoice = document.getElementById(choiceCat)
    seeMoreRated.addEventListener("click", function () {
        changeButton(ratedCat)
    })
    seeMoreHorror.addEventListener("click", function () {
        changeButton(horrorCat)
    })
    seeMoreBiography.addEventListener("click", function () {
        changeButton(biographyCat)
    })
    seeMoreChoice.addEventListener("click", function () {
        changeButton(choiceCat)
    })
}


// Change description of button when clicking it
function changeButton (category) {
    let seeMore1 = document.getElementById(category)
    let stateButton = seeMore1.textContent
    if (stateButton === "Voir plus") {
        let cat = foundCategory(category)
        changeStyleTablet("Voir plus", cat)
        seeMore1.textContent = "Voir moins"
    } else {
        let cat = foundCategory(category)
        changeStyleTablet("Voir moins", cat)
        seeMore1.textContent = "Voir plus";
        }
}


// Find a category class matched with button click
function foundCategory(categoryId) {
    if (categoryId === "show-hidden-rated") {
        const classCatRated = ".grid-rated"
        return classCatRated
    } else if (categoryId === "show-hidden-horror") {
        const classCatHorror = ".grid-horror"
        return classCatHorror
    } else if (categoryId === "show-hidden-biography") {
        const classCatBiography = ".grid-biography"
        return classCatBiography
    } else if (categoryId === "show-hidden-choice") {
        const classCatChoice = ".grid-choice"
        return classCatChoice
    } else {
        console.log("aucune grille a déroulé trouvé")
    }
}


// Modify CSS when modal window opened
function modalCss (openClose) {
    const openModal = document.querySelector(".modal")
    const overlay = document.getElementById("overlay")
    if (openClose === "open") {
        openModal.style.display = "block"
        overlay.style.display = "block"
    } else if (openClose === "close" || openClose === "close-resp") {
        openModal.style.display = "none"
        overlay.style.display = "none"
    }
}

// Open window modal when user click on button
async function modal () {
    const buttonModal = document.querySelectorAll(".open-modal")
    const buttonModalClose = document.getElementById("close-button")
    const buttonModalCloseResp = document.getElementById("close-button-mobile")
    buttonModal.forEach(buttonModal => {
        buttonModal.addEventListener("click",async function() {
            modalCss("open")
            let buttonId = this.id
            let movieTitle = getTitle(buttonId)
            let idMovie = await getIdMovie(movieTitle)
            let dataMovie = await getDataModal(idMovie)
            giveDataModal(dataMovie)
        })
    })
    buttonModalClose.addEventListener("click", function() {
        modalCss("close")
    })
    buttonModalCloseResp.addEventListener("click", function() {
        modalCss("close-resp")
    })
}


// Get title with id of button
function getTitle (buttonId) {
    const button = document.getElementById(buttonId)
    const gridTitleBtn = ".grid-film__film-title"
    const gridFilmItem = ".grid-film__item"
    const bestTitleFilm = ".bestfilm__title"
    const container = ".best-film__container-title"
    try {
        const movieTitle = button.closest(gridFilmItem)
        .querySelector(gridTitleBtn).textContent
        return movieTitle
    }
    catch {
        const movieTitle = button.closest(container)
        .querySelector(bestTitleFilm).textContent
        return movieTitle
    }
}


// Get request of API with movie title for getting id of movie
async function getIdMovie (movieTitle) {
    let newMovieTitle
    if (movieTitle.includes("&")) {
        newMovieTitle = movieTitle.replace(/&/g, "%26")
        newMovieTitle = newMovieTitle.replace(/ /g, "+")
    } else {
        newMovieTitle = movieTitle.replace(/ /g, "+")
    }
    let url = "http://localhost:8000/api/v1/titles/?title="+newMovieTitle
    let data = await getData(url)
    let idMovie = data.results[0].id
    return idMovie
}


// Get request of API with id movie to getting infos of movie
async function getDataModal (idMovie) {
    let urlMovie = "http://localhost:8000/api/v1/titles/"
    let listData = []
    let listInfos = ["image_url", "title", "genres", "year", "classification",
    "imdb_score", "directors", "actors", "duration", "countries",
    "worldwide_gross_income", "long_description"]
    await getData(urlMovie + idMovie)
    .then(function(data) {
        listInfos.forEach(info => {
            let getInfos = data[info]
            if (getInfos === null || getInfos === undefined) {
                getInfos = "N/A"
                listData.push(getInfos)
            } else {
                listData.push(getInfos)
            }
        })
    })
    return listData
}


// Add data in modal window
function giveDataModal (data) {
    const imgMobile = document.getElementById("img-modal-mobile")
    const img = document.getElementById("img-modal")
    const title = document.getElementById("modal-title")
    const dateGenre = document.getElementById("date-genre")
    const duration = document.getElementById("age-duration-countrie")
    const imdbScore = document.getElementById("score-imdb")
    const reals = document.getElementById("reals")
    const actors = document.getElementById("actors")
    const grossIncome = document.getElementById("gross-income")
    const desc = document.getElementById("modal-desc")
    const genre = data[2].join(", ")
    const realisator = data[6].join(", ")
    const actor = data[7].join(", ")
    const resolution = window.innerWidth
    if (resolution <= 1024) {
        let imgHdMobile = imageHd(data[0])
        imgMobile.src = imgHdMobile
    } else {
        let imgHd = imageHd(data[0])
        img.src = imgHd
    }
    title.textContent = data[1]
    dateGenre.textContent = data[3] + ", " + genre
    duration.textContent = data[4]+", "+data[8]+" minutes "+"("+data[9]+")"
    grossIncome.textContent = "Recette Box office : " + data[10] + "$"
    imdbScore.textContent = "IMDB score : " + data[5] + "/10"
    reals.textContent = realisator
    desc.textContent = data[11]
    actors.textContent = actor
}


// Get a data found with url indicate
async function getData(url) {
    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error(response.status);
        }
        return await response.json();
    } catch (error) {
        return "error";
    }
}


// Data to be loaded when DOM is ready
function loaded() {
        document.addEventListener("DOMContentLoaded", loadData())
}


// Data to be loaded
function loadData() {
    getDataBestFilm()
    getDataGridFilm(filterImdb, gridRated, gridImg, gridTitle)
    getDataGridFilm(filterHorror, gridHorror, gridImg, gridTitle)
    getDataGridFilm(filterBiography, gridBiography, gridImg, gridTitle)
    giveGenres()
    console.log("DOM loaded")
}


function main() {
    loaded()
    getCategoryChoose()
    seeMore()
    modal()
}

main()