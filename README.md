JustStreamIt
============

Description
-----------

JustStreamIt est un site web permettant de visualiser des films en temps réel.

Il sera possible de rechercher des films en fonction de leurs genre (catégorie).

Fonctionnalités
---------------

Le site web affichera par défaut, plusieurs listes de films de la sorte : 
* Meilleur film : film possèdant la meilleure note IMDB
* Films les mieux notés : films les mieux notés selon le score IMDB
* Horreur : Montre les films les mieux notés de la catégorie Action
* Biography : Montre les films les mieux notés de la catégorie Aventure
* Catégorie à choisir : Affichera les films les mieux notés en fonction de la catégorie selectionné par l'utilisateur

Information des films
---------------------

Un bouton "détails" est disponible pour chaque film, pour avoir plus d'informations à son sujet il suffit de cliquer dessus,
dés lors, une fenêtre modale s'ouvrira pour afficher toutes les informations relatives au film. 

Ci-dessous, une liste des informations disponibles :

* Titre du film
* Image de présentation du film
* Genre complet (Horreur, Dramatique, Documentaire etc...)
* Date de sortie
* Classification (10+, 18+, etc...)
* Score IMDB
* Réalisateur
* Acteurs
* Durée
* Pays d'origine
* Recette au box-office
* Résumé du film

*Des informations peuvent ne pas être disponible, si tel est le cas, une mention N/A apparaitra à la place de celle ci*

Techniques
----------

### Reactive

Le site web sera entièrement responsive, et sera donc utilisable via les plateformes suivantes :
* Mobile
* Tablette
* Ordinateur (PC) 

*Résolution PC basé sur HD (1920x1080), des low-résolution seront également pris en compte*

### Affichage

En fonction du support l'affichage du nombre de film sera adapté

* #### Mobile : La version mobile montrera 2 films (4 seront cachés)
* #### Tablette : La version tablette montrera 4 films (2 seront cachés)
* #### Ordinateur (PC) : La version PC montrera 6 films

*Les films cachés pourront être affichés en cliquant sur un bouton "Voir plus"*

### Installation

Le site web n'ayant pas été déployé en ligne sur un serveur vous pouvez suivre les méthodes suivantes pour y accéder localement :

Tous d'abord, importez le projet en le clonant :

```
git clone https://gitlab.com/Hekynox/juststreamit.git
```

Vous aurez également besoin d'importer le projet contenant l'API, rendez vous donc sur la page de celui-ci : https://github.com/OpenClassrooms-Student-Center/OCMovies-API-EN-FR
Puis suivez les instructions contenu dans le README, vous pouvez passer aux prochaines instructions du README du projet **JustStreamIt** après avoir démarré le serveur de l'API.

Une fois ceci fait, vous pouvez choisir d'ouvrir le projet de deux manière pour le déployer localement : 

* Utiliser le mode "Live server" de votre IDE préféré si il en a la possibilité
***Sur VSCode cliquer sur "Go Live" dans la petite barre du bas, si non présente, installer le plugin "Live Server"***

**OU**

* Rendez vous dans le fichier du projet, puis aller dans le dossier HTML, enfin faites un clique droit sur le fichier index.html (ou double cliquer dessus), 
puis selectionner **ouvrir avec** et selectionner votre navigateur préféré.
